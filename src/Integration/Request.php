<?php

namespace Skyeng\Integration;

use JsonSerializable;

class Request implements JsonSerializable
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var array<string, mixed>
     */
    private $parameters;

    /**
     * @param string $path
     * @param array<string, mixed> $parameters
     */
    public function __construct(string $path, array $parameters = [])
    {
        $this->path = $path;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return md5(json_encode($this));
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'path' => $this->getPath(),
            'parameters' => $this->getParameters(),
        ];
    }
}
