<?php

namespace Skyeng\Integration;

class Response
{
    /**
     * @var array Parsed result
     */
    private $result;

    /**
     * @param array $result
     */
    public function __construct(array $result)
    {
        $this->result = $result;
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
