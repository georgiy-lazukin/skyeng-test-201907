<?php

namespace Skyeng\Integration;

use Psr\Cache\CacheException;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Client\NetworkExceptionInterface;
use Psr\Http\Client\RequestExceptionInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;
use Throwable;

class Client
{
    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var int Cache lifetime, seconds
     */
    private $cacheLifetime;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ClientInterface $httpClient
     * @param CacheItemPoolInterface $cache
     * @param int $cacheLifetime Cache lifetime, seconds
     * @param LoggerInterface $logger
     */
    public function __construct(
        ClientInterface $httpClient,
        CacheItemPoolInterface $cache,
        int $cacheLifetime,
        LoggerInterface $logger
    ) {
        $this->httpClient = $httpClient;
        $this->cache = $cache;
        $this->cacheLifetime = $cacheLifetime;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     * @throws Throwable
     */
    public function request(Request $request): Response
    {
        try {
            // get cache item
            $cacheItem = $this->getCacheItem($request);

            if ($cacheItem->isHit()) {
                $cacheValue = $cacheItem->get();
                if (!is_array($cacheValue)) {
                    throw new Exception("Invalid cache value for key {$cacheItem->getKey()}", 0, null, [
                        'key' => $cacheItem->getKey(),
                    ]);
                }

                $response = new Response($cacheValue);
            } else {
                $httpRequest = $this->createHttpRequest($request);

                try {
                    $httpResponse = $this->httpClient->sendRequest($httpRequest);
                } catch (RequestExceptionInterface | NetworkExceptionInterface $error) {
                    $httpRequest = $error->getRequest() ?: $httpRequest;

                    throw new Exception('Http request error', 0, $error, [
                        'uri' => $httpRequest->getUri(),
                        'method' => $httpRequest->getMethod(),
                        // ...
                    ]);
                }

                // check http status
                $httpStatusCode = $httpResponse->getStatusCode();
                if (($httpStatusCode < 200) || ($httpStatusCode >= 300)) {
                    throw new Exception("Invalid http status code {$httpStatusCode}", 0, null, [
                        'uri' => $httpRequest->getUri(),
                        'method' => $httpRequest->getMethod(),
                        // ...
                    ]);
                }

                // TODO: inject http-response parser
                $result = json_decode((string)$httpResponse->getBody(), true);

                $errorCode = json_last_error();
                if ($errorCode !== JSON_ERROR_NONE) {
                    throw new Exception("Json parse error #{$errorCode}", 0, null, [
                        'json_error' => json_last_error_msg(),
                    ]);
                }

                $response = new Response($result);

                // save cache item
                $this->saveCacheItem($response, $cacheItem);
            }
        } catch (Exception $error) {
            $this->logger->critical($error->getMessage(), $error->getContext());

            throw $error;
        } catch (Throwable $error) {
            $this->logger->critical("Integration fatal error: {$error->getMessage()}");

            throw $error;
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return RequestInterface
     */
    private function createHttpRequest(Request $request): RequestInterface
    {
        // TODO: implement createHttpRequest. Build http request by integration request object
    }

    /**
     * @param Request $request
     * @return CacheItemInterface
     * @throws Exception
     */
    private function getCacheItem(Request $request): CacheItemInterface
    {
        $cacheKey = $request->getId();

        try {
            $cacheItem = $this->cache->getItem($cacheKey);
        } catch (CacheException $error) {
            throw new Exception(
                "Cache error: {$error->getMessage()}",
                0,
                $error,
                [
                    'key' => $cacheKey,
                ]
            );
        }

        return $cacheItem;
    }

    /**
     * @param Response $response
     * @param CacheItemInterface $cacheItem
     */
    private function saveCacheItem(Response $response, CacheItemInterface $cacheItem)
    {
        // save cache
        $cacheItem
            ->set($response->getResult())
            ->expiresAfter($this->cacheLifetime);

        $this->cache->save($cacheItem);
    }
}
