<?php

namespace Skyeng\Integration;

use Throwable;

class Exception extends \Exception
{
    /**
     * @var array
     */
    private $context = [];

    /**
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param array $context
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null, array $context = [])
    {
        parent::__construct($message, $code, $previous);

        $this->context = $context;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }
}
