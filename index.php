<?php

use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Log\NullLogger;
use Skyeng\Integration;

/** @var CacheItemPoolInterface $cache */
$cache = null;

/** @var ClientInterface $httpClient */
$httpClient = null;
// TODO: configure http client: auth, timeout, ...

$logger = new NullLogger();

$client = new Integration\Client(
    $httpClient,
    $cache,
    60, // seconds
    $logger
);

$response = $client->request(
    new Integration\Request('users', [
        'criteria' => ['email' => 'green@mail.ru'],
    ])
);
